# Match Football API

## Description

This project was bootstrapped with [Nestjs](https://github.com/nestjs/nest).

## DATABASE manual configuration

### database version 10.4.14-MariaDB

#### create a database called "footballapi", then execute the scripts below

```
TABLES FOR THE API

CREATE TABLE `footballapi`.`match_status` ( `id` INT NOT NULL AUTO_INCREMENT , `status_name` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
INSERT INTO `match_status` (`id`, `status_name`) VALUES ('1', 'SCHEDULED'), ('2', 'FINISHED');  

CREATE TABLE `footballapi`.`team` ( `id` INT NOT NULL, `team_name` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `footballapi`.`match` 
( 
`id` INT NOT NULL, 
`date` DATE,
`winner` VARCHAR(50),
`score` VARCHAR(50),
`match_status_id` INT(11) NOT NULL,
`home_team` INT(11) NOT NULL,
`away_team` INT(11) NOT NULL,
foreign key (`match_status_id`) references match_status(id) on delete cascade on update cascade, 
foreign key (`home_team`) references team(id) on delete cascade on update cascade, 
foreign key (`away_team`) references team(id) on delete cascade on update cascade, 
PRIMARY KEY (`id`)) ENGINE = InnoDB; 

```

Add the access information from the database on the .env file as in the example below

```
DATABASE=footballapi
DATABASE_PORT=3306
DATABASE_HOST=localhost
USERNAMEDB=root
PASSWORD=''
```

## DATABASE docker configuration
Follow the instructions in the project [dockerconfig](https://gitlab.com/estigon/football-matches-database).

## Installation

```bash
In the project directory execute
$ npm install
```

## Running the app

```bash
In the project directory execute

# development
$ npm run start

# watch mode
$ npm run start:dev
