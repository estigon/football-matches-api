import { Controller, Get, Query } from '@nestjs/common';
import { PaginatedMatchRequestDto } from 'src/match/dto';
import { MatchService } from 'src/match/services/match/match.service';

@Controller('api/match')
export class MatchController {

    constructor(
        private matchService: MatchService
    ){}

    @Get()
    getPaginatedMatches(@Query() pagination: PaginatedMatchRequestDto) {
        return this.matchService.getPaginatedMatches(pagination);
    }
}
