import { Module } from '@nestjs/common';
import { MatchService } from './services/match/match.service';
import { MatchController } from './controllers/match/match.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Match, MatchStatus, Team } from './entities';

@Module({
  imports: [
    TypeOrmModule.forFeature([Match,MatchStatus,Team])
  ],
  providers: [MatchService],
  controllers: [MatchController]
})
export class MatchModule {}
