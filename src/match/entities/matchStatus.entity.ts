import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class MatchStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({name: 'status_name'})
  statusName: string;
}