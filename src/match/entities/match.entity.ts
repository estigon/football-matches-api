import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { MatchStatus } from './matchStatus.entity';
import { Team } from './team.entity';

@Entity()
export class Match {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @Column()
  winner?: string;

  @Column()
  score?: string;

  @OneToOne(() => MatchStatus)
  @JoinColumn({ name: 'match_status_id' })
  matchStatus: MatchStatus;

  @OneToOne(() => Team)
  @JoinColumn({ name: 'home_team' })
  homeTeam: Team;

  @OneToOne(() => Team)
  @JoinColumn({ name: 'away_team' })
  awayTeam: Team;
}