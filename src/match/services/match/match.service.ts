import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToInstance } from 'class-transformer';
import { PaginatedMatchRequestDto, PaginatedMatchResponseDto } from 'src/match/dto';
import { MatchesDto } from 'src/match/dto/paginatedMatchResponse.dto';
import { Match } from 'src/match/entities';
import { Repository } from 'typeorm';

@Injectable()
export class MatchService {

    constructor(
        @InjectRepository(Match) private matchRepository : Repository<Match>,
    ){}

    async getPaginatedMatches({ limit, offset }: PaginatedMatchRequestDto): Promise<PaginatedMatchResponseDto> {

        const response = await this.matchRepository.findAndCount({
            relations: {
                awayTeam: true,
                homeTeam: true,
                matchStatus: true
            },
            skip: offset,
            take: limit
        });

        const [matchesRaw, total] = response;

        if(total === 0) {
            throw new NotFoundException('There are not matches yet.');
        }

        const matches =  plainToInstance(MatchesDto, matchesRaw);
        const apiResponse: PaginatedMatchResponseDto = {matches, total}

        return apiResponse;
    }
}
