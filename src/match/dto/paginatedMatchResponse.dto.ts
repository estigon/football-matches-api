export class PaginatedMatchResponseDto {
    matches: MatchesDto[];
    total: number;
}

class TeamDto {
    id: number;
    teamName: string;
}

class MatchStatusDto {
    id: number;
    statusName: string;
}

export class MatchesDto {
    id: number;
    date: string;
    winner?: string;
    score?: string;
    awayTeam: TeamDto;
    homeTeam: TeamDto;
    matchStatus: MatchStatusDto;
}