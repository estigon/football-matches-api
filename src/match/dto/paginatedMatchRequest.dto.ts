import { IsNumber, IsPositive } from "class-validator";

export class PaginatedMatchRequestDto {
    @IsNumber()
    @IsPositive()
    limit: number;

    @IsNumber()
    @IsPositive()
    offset: number;
}